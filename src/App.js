import React from "react";
import "./App.css";
import Counter from "./components/counter.js";
import Count from "./components/count.jsx";
function App() {
  return (
    <div className="App">
      <h1>HELLO COUNTER</h1>
      <Counter />
      <Count />
    </div>
  );
}

export default App;
